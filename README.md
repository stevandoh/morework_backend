Deploying App and Other Goodies
===============================

Public - Private Key authorization (Run Once)
---------------------------------------------
Ref: https://help.ubuntu.com/community/SSH/OpenSSH/Keys

This is very important to allow you to login to the server without entering your password everytime. You will need to generate your keys on your local machine first, if you have not already done so.

```sh
$ ssh-keygen -t rsa
```

Next run this command to copy your public key to your server. You will be asked
to enter your password once for copying.

```sh
$ fab utils.setup_ssh_pubkey_auth:morework
```

NOTE: Please always make sure you are working under staging branch on local machine

```sh
$ git checkout staging
```

Setup Project Git Repository (Run Once)
---------------------------------------
You need to setup a new branch directly on app server to receive pushes. We call
this branch staging. This branch needs to be setup both on server and local repo. 
We also need to setup a git remote to the server

```sh
$ fab deploy.setup_project_git:morework 
```

Deploy Project (Run Always)
---------------------------
### Tasks:

 - Push project to server
 - Merge staging branch to master branch
 - Install requirements
 - Migrate database
 - Restart server
    
```sh
$ fab deploy:morework
```
