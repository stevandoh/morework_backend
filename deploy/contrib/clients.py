import os
from fabric.contrib import files
from fabric.api import run, cd 


class ClientObj(object):

    def __init__(self, client_settings, dev_settings={}):
        self.host = client_settings['HOST']
        self.user_home_path = client_settings['USER_HOME_PATH']
        self.py_version = client_settings['PYTHON_VERSION']
        self.libs_dir_path = client_settings['LIBS_DIR_PATH']
        self.proj_dir_path = client_settings['PROJ_DIR_PATH']
        self.django_settings_path = client_settings['DJANGO_SETTINGS_PATH']
        self.remote_alias = client_settings.get('REMOTE_ALIAS', None)
        self.local_project_home = dev_settings.get('PROJECT_HOME', None)

    def get_py_major_version(self):
        return '.'.join(self.py_version.split('.')[:2])

    def get_py_folder_name(self):
        return 'python' + self.get_py_major_version().lower().replace('.', '')

    def get_full_libs_path(self):
        return '%s/%s' % (self.user_home_path, self.libs_dir_path)

    def get_full_proj_path(self):
        return '%s/%s' % (self.user_home_path, self.proj_dir_path)

    def get_local_proj_path(self):
        if not self.local_project_home:
            raise Exception("Please provide value for dev_settings")
        return os.path.join(os.path.dirname(__file__), self.local_project_home)

    def get_remote_alias(self):
        alias = self.remote_alias
        if not alias:
            alias = self.host
            # Clear username from host
            if '@' in alias:
                alias = alias.split('@')[1]

            if '.' in alias:
                # Check if host is an ip, by inspecting datatype of last section after .
                last = alias.split('.')[-1]
                try: 
                    int(last)
                    is_int = True
                except:
                    is_int = False

                if not is_int:
                    # It is a fully qualified domain name, so we just pick the last
                    # 2 sections. Eg: app.example.com -> example.com
                    xs = alias.split('.')[-2:]
                    alias = '.'.join(xs)

            alias = alias.replace('.', '_')

        return alias


    def get_py_path(self, add_py_folder=True, add_version=True):
        txt = ''
        if self.libs_dir_path:
            txt += self.get_full_libs_path()

        if add_py_folder:
            txt += '/%s/bin/' % self.get_py_folder_name()

        txt += 'python'

        if add_version:
            txt += self.get_py_major_version()
        
        return txt

        # return '%s/%s/bin/python%s' % (
        #                     self.get_full_libs_path(), 
        #                     self.get_py_folder_name(),
        #                     self.get_py_major_version()
        #                 ) 

    def get_pip_path(self, add_py_folder=True, add_version=True):
        txt = ''
        if self.libs_dir_path:
            txt += self.get_full_libs_path()

        if add_py_folder:
            txt += '/%s/bin/' % self.get_py_folder_name()

        txt += 'pip'

        if add_version:
            txt += self.get_py_major_version()
        
        return txt


    def pip_install(self, lib_pip_name, lib_installed_name): 
        install_path = '%s/%s/lib/python%s/site-packages/%s' % (
                                        self.get_full_libs_path(), 
                                        self.get_py_folder_name(),
                                        self.get_py_major_version(),
                                        lib_installed_name
                                    ) 
        
        if files.exists(install_path):
            print ('%s already installed. Skipping ...' % lib_installed_name) 
        else:
            run('%s install %s' % (self.get_pip_path(), lib_pip_name)) 


    def manual_install(self, lib_path, lib_installed_name): 
        install_path = '%s/%s/lib/python%s/site-packages/%s' % (
                                        self.get_full_libs_path(), 
                                        self.get_py_folder_name(),
                                        self.get_py_major_version(),
                                        lib_installed_name
                                    ) 
        
        if files.exists(install_path):
            print ('%s already installed. Skipping ...' % lib_installed_name) 
        else:
            with cd(lib_path):
                run('%s setup.py install' % self.get_py_path()) 
