import os
from fabric.api import task, run, cd, local, lcd, settings
from fabric.contrib import files

from constants import CLIENT_SETTINGS, DEV_SETTINGS
from contrib.clients import ClientObj


        
@task
def setup_project_git(client, git_branch='staging'):
    c_obj = ClientObj(CLIENT_SETTINGS[client], DEV_SETTINGS)

    remote_proj_path = c_obj.get_full_proj_path()
    local_proj_path = c_obj.get_local_proj_path() 

    # Setup git branch on remote server
    with cd(remote_proj_path):
        result = run('git branch')
        if result.find(git_branch) > 0:
            print ('Branch "%s" already exists on server. Skipping ...' % git_branch)
        else:
            result = run('git branch %s' % git_branch)

    # Setup git branch on local machine
    with lcd(local_proj_path):
        result = local('git branch', capture=True)
        if result.find(git_branch) > 0:
            print ('Branch "%s" already exists on local. Skipping ...' % git_branch)
        else:
            result = local('git branch %s' % git_branch)

    # Setup git remote for local repo to point to remote repo
    remote_alias = c_obj.get_remote_alias()
    remote_git_url = '%s:%s' % (c_obj.host, remote_proj_path)
    with lcd(local_proj_path):
        result = local('git remote', capture=True)
        if result.find(remote_alias) > 0:
            print ('Remote "%s" already exists on local. Skipping ...' % remote_alias)
        else:
            result = local('git remote add %s %s' % (remote_alias, remote_git_url))


@task
def push_project_files(client, git_branch='staging'):
    c_obj = ClientObj(CLIENT_SETTINGS[client], DEV_SETTINGS)

    local_proj_path = c_obj.get_local_proj_path()
    remote_alias = c_obj.get_remote_alias()

    if os.path.isdir(local_proj_path):

        local_folder = os.path.join(local_proj_path, '..') 
        with lcd(local_folder):
            local('git push %s %s' % (remote_alias, git_branch))

    else:
        print ("Project home path not found.")


@task
def install_project_dependencies(client, git_branch='staging'):
    c_obj = ClientObj( CLIENT_SETTINGS[client] ) 

    remote_proj_path = c_obj.get_full_proj_path()
    requirements_file = '%s/requirements.txt' % remote_proj_path

    with cd(remote_proj_path):
        if not files.exists(requirements_file):
            run('git merge %s' % git_branch)

        run('git reset --hard')
        run('git merge %s' % git_branch)

    run('%s install -r %s' % (c_obj.get_pip_path(add_py_folder=False), 
                              requirements_file))


@task
def migrate_db_and_static(client):
    c_obj = ClientObj( CLIENT_SETTINGS[client] ) 

    remote_proj_path = c_obj.get_full_proj_path()
    manage_file = '%s/manage.py' % remote_proj_path 
    python_path = c_obj.get_py_path(add_py_folder=False)

    with cd(remote_proj_path):
        run('%s %s migrate --settings=%s' % (python_path, manage_file,  
                                             c_obj.django_settings_path) 
                                            )

        run('%s %s collectstatic --noinput --settings=%s' % (
                                                python_path,  
                                                manage_file,
                                                c_obj.django_settings_path)
                                            ) 

@task
def restart_app_server(client):
    c_obj = ClientObj( CLIENT_SETTINGS[client] ) 

    remote_proj_path = c_obj.get_full_proj_path()

    with cd(remote_proj_path):
        run('../apache2/bin/restart')



@task(default=True)
def full_deploy(client):
    push_project_files(client)
    # install_project_dependencies(client)
    migrate_db_and_static(client)
    restart_app_server(client)