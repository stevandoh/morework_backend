from rest_framework import serializers
# from rest_auth.serializers import TokenSerializer, UserDetailsSerializer

from webapps.modules.core.entities.models import Entity
from webapps.modules.api.v1f.tools.custom_fields import GenderField, EntityTypeField, AgeRangeField


class EntitySerializer(serializers.ModelSerializer):
	# gender = GenderField()
	# type = EntityTypeField()
	# age_range = AgeRangeField()

	class Meta:
		model = Entity
		fields =  ['id','name','type','website','gender','type','age_range',
				  'date_created','country_of_residence','email', 'user', 'phone', 'description',
				  'date_modified','date_removed','date_active','guid','created_by','modified_by']
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
                         'modified_by': {'default': serializers.CurrentUserDefault()}}




# class MyUserDetailsSerializer(UserDetailsSerializer):

#     class Meta(UserDetailsSerializer.Meta):
#         fields = UserDetailsSerializer.Meta.fields + (
#             'is_active', 'is_staff', 'is_superuser', 'date_joined', 'last_login',)


# class MyTokenSerializer(TokenSerializer): 
#     user = MyUserDetailsSerializer()
#     entity = serializers.SerializerMethodField()

#     def get_entity(self, obj): 
#         return EntitySerializer(obj.user.profile, context=self.context).data 

#     class Meta(TokenSerializer.Meta):
#         fields = TokenSerializer.Meta.fields + ('user', 'entity',)


