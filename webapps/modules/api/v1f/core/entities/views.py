from rest_framework import viewsets
from .serializers import EntitySerializer
from webapps.modules.core.entities.models import Entity
from django_filters.rest_framework import DjangoFilterBackend


class EntityViewSet(viewsets.ModelViewSet):
    queryset = Entity.objects.all()
    serializer_class = EntitySerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ( 'age_range', 'type', 'name', 'user',)
    