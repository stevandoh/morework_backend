import django_filters
from webapps.modules.core.jobs.models import JobPost


class JobPostFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = JobPost 
        fields = {
            'name': ['exact', 'icontains'],
            'venue': ['exact'],
            'type': ['exact'],
            'entity': ['exact'],
            'source': ['exact'],
            'start_date': ['exact', 'gte', 'lte'],
            'end_date': ['exact', 'gte', 'lte'],
        }