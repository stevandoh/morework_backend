from rest_framework import serializers
from webapps.modules.core.jobs.models import JobPost, Compensation
from webapps.modules.api.v1f.tools.custom_fields import VenueTypeField, JobTypeField, CompensationTypeField
from webapps.modules.api.v1f.tools.serializers import AbstractHyperlinkedModelSerializer
from .filters import JobPostFilter
# from django_countries.serializer_fields import CountryField

class JobPostSerializer(serializers.ModelSerializer):
	# type = JobTypeField()
	# venue = VenueTypeField()
	# compensation_type = CompensationTypeField()
	date_created = serializers.DateTimeField(read_only=True)
	date_modified = serializers.DateTimeField(read_only=True)
	date_active = serializers.DateTimeField(read_only=True)

	class Meta:
		model = JobPost
		# fields = '__all__'
		# 
		fields =  ['id','url','name','description','area','venue','type','how_to_apply',
					'start_date','end_date','entity','date_created','remote_id', 'source',
				  'date_modified','date_removed','date_active','guid','created_by','modified_by']
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
		 				'modified_by': {'default': serializers.CurrentUserDefault()},}

class CompensationSerializer(serializers.ModelSerializer):

	class Meta:
		model = Compensation
		fields = '__all__' 
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
		 				'modified_by': {'default': serializers.CurrentUserDefault()},}
	