from rest_framework import serializers
from webapps.modules.core.locations.models import Location
from webapps.modules.api.v1f.tools.custom_fields import LocationTypeField
from rest_framework_recursive.fields import RecursiveField

class LocationSerializer(serializers.ModelSerializer):
	# parent = RecursiveField(read_only=True)
	type = LocationTypeField()

	class Meta:
		model = Location
		fields =  ['id','name','url','parent','is_default','type','sort','iso3','iso2','alias','alias','latitude',
				   'zip_code','currency_code','date_created','date_modified','date_removed',
				   'date_active','guid','created_by','modified_by']
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
		 				'modified_by': {'default': serializers.CurrentUserDefault()},}


