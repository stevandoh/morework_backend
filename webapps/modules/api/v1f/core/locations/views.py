from rest_framework import viewsets
from webapps.modules.api.v1f.core.locations.serializers import LocationSerializer
from webapps.modules.core.locations.models import Location


class LocationViewSet(viewsets.ModelViewSet):

    queryset = Location.objects.all()
    serializer_class = LocationSerializer
 