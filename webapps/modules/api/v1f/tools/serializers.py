
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from webapps.modules.tools.models import BaseModel, Choice
from webapps.utils.util_box import  get_section
from django.conf import settings
from django.contrib.auth.models import User

class BaseModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseModel
        fields = ('name','email','description','picture','sort','date_created','date_modified','slug')

class AbstractHyperlinkedModelSerializer(serializers.ModelSerializer):
    created_by = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    modified_by = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    def __init__(self, *args, **kwargs): 
        super(AbstractHyperlinkedModelSerializer, self).__init__(*args, **kwargs) 
        self.fields['created_by'].required = False
        self.fields['created_by'].allow_blank = True
        self.fields['created_by'].allow_null = True
        self.fields['modified_by'].required = False
        self.fields['modified_by'].allow_blank = True
        self.fields['modified_by'].allow_null = True


class ChoiceSerializer(serializers.ModelSerializer):

    # parent = RecursiveField(read_only=True)
    
    class Meta:
        model = Choice
      #  fields = '__all__'
        fields =  ['id','url','date_created','name','slug',
                  'date_modified','date_removed','date_active','guid','created_by','modified_by','section']
        extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
                         'modified_by': {'default': serializers.CurrentUserDefault()}}
        


# class EntityViewSet(viewsets.ModelViewSet):

#     queryset = Entity.objects.all()
#     serializer_class = EntitySerializer
#     filter_backends = (DjangoFilterBackend,)
#     filter_fields = ( 'age_range','type','country_of_residence')