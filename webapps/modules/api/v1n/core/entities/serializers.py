from rest_framework import serializers
from webapps.modules.core.entities.models import Entity
from webapps.modules.api.v1n.tools.serializers import ChoiceSerializer
from webapps.modules.api.v1n.core.locations.serializers import LocationSerializer

class EntitySerializer(serializers.ModelSerializer):

	gender = ChoiceSerializer()
	type = ChoiceSerializer()
	age_range = ChoiceSerializer()
	country_of_residence = LocationSerializer()
	# 
	
	class Meta:
		model = Entity
		fields =  ['id','url','name','type','website','gender','type','age_range',
				  'date_created','country_of_residence','email',
				  'date_modified','date_removed','date_active','guid','created_by','modified_by']
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
                         'modified_by': {'default': serializers.CurrentUserDefault()}}





