from rest_framework import serializers
from webapps.modules.core.locations.models import Location
from webapps.modules.api.v1f.tools.custom_fields import LocationTypeField
from rest_framework_recursive.fields import RecursiveField
from webapps.modules.api.v1n.tools.serializers import ChoiceSerializer

class LocationSerializer(serializers.ModelSerializer):
	parent = RecursiveField(read_only=True)
	type = ChoiceSerializer()

	class Meta:
		model = Location
		fields =  ['id','url','name','parent','is_default','type','sort','iso3','iso2','alias','alias','latitude',
				   'zip_code','currency_code','date_created','date_modified','date_removed',
				   'date_active','guid','created_by','modified_by']
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
		 				'modified_by': {'default': serializers.CurrentUserDefault()},}


