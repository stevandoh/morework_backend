from rest_framework import serializers
from webapps.modules.core.tech.models import Technology
from webapps.modules.api.v1n.tools.serializers import ChoiceSerializer


class TechnologySerializer(serializers.ModelSerializer):

	venue = ChoiceSerializer()
	technology_type = ChoiceSerializer()
	

	class Meta:
		model = Technology
		fields = '__all__'

