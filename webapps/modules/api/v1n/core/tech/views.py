from rest_framework import viewsets
from .serializers import  TechnologySerializer
from webapps.modules.core.tech.models import Technology

class TechnologyViewSet(viewsets.ModelViewSet):
	queryset = Technology.objects.all()
	serializer_class = TechnologySerializer
 
# class InterestViewSet(viewsets.ModelViewSet):
# 	queryset = Interest.objects.all()
# 	serializer_class = InterestSerializer