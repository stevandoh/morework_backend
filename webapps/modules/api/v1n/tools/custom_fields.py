from rest_framework import serializers
from webapps.modules.tools.models import  Choice
from webapps.utils.util_box import  get_section
#Custom fields
class GenderField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
    	queryset = Choice.objects.filter(section=get_section('entities.entity-gender'))
    	return queryset

class EntityTypeField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
    	queryset = Choice.objects.filter(section=get_section('entities.entity-type'))
    	return queryset

class AgeRangeField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
    	queryset = Choice.objects.filter(section=get_section('entities.entity-age-range'))
    	return queryset

class VenueTypeField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
    	queryset = Choice.objects.filter(section=get_section('jobs.job-venue'))
    	return queryset

class JobTypeField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
    	queryset = Choice.objects.filter(section=get_section('jobs.job-type'))
    	return queryset