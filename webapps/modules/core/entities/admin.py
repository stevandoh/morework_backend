from django.contrib import admin

# Register your models here.
from .models import Entity
from webapps.modules.tools.admin import BaseAdmin

@admin.register(Entity)
class EntityAdmin(BaseAdmin):
    list_display = ('name','country_of_residence','type','category')
    list_filter = ('name','country_of_residence','type','category')
    search_fields = ('name','country_of_residence','type','category')  
    prepopulated_fields = {"slug": ("name",)}
	