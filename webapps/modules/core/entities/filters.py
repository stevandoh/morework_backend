from .models import Entity
from rest_framework import viewsets
import rest_framework_filters as filters
from webapps.modules.tools.models import  Choice
from webapps.utils.util_box import  get_section


# class ManagerFilter(filters.FilterSet):
#   gender = filters.RelatedFilter(ManagerFilter, name='manager', queryset=Manager.objects.all())
#     class Meta:
#         model = Manager
#         fields = {'country_of_residence': ['exact', 'in', 'startswith'],
#                   'gender':['exact'],
#                   'age_range': ['exact']}


# class DepartmentFilter(filters.FilterSet):
#     manager = filters.RelatedFilter(ManagerFilter, name='manager', queryset=Manager.objects.all())

#     class Meta:
#         model = Department
#         fields = {'name': ['exact', 'in', 'startswith']}



# class EntityFilter(filters.FilterSet):
#     country_of_residence = filters.ModelChoiceFilter(
#       queryset=Choice.objects.filter(section=get_section('entities.entity-age-range')))   



