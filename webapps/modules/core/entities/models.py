from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from datetime import datetime
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from webapps.modules.tools.models import VBaseModel, ContactBaseModel ,Choice
from webapps.utils.util_box import gen_tuple_from_dict, get_section
from webapps.modules.core.tech.models import Technology
from webapps.modules.core.locations.models import Location


"""
    Model to display an entity field and provide extra information.

    :country_of_residence: location of the entity.
    :gender: if  the entity is an individual the gender should be specified.
    :registraion_type: Type of registration either individual or business.
    :age_range: age range of the entity.
"""
class Entity(VBaseModel, ContactBaseModel):
    name = models.CharField(max_length=512, null=False, blank=False)
    other_names = models.CharField(max_length=512, null=True, blank=True)
    slug = models.SlugField(null=True)
    picture = models.ImageField(null=True, blank=True)
    country_of_residence = models.ForeignKey(Location, blank=True, null=True)     
    user = models.OneToOneField(User, null=True, blank=True)           
    gender = models.ForeignKey(Choice, null=True, blank=True, 
                               help_text='Choose your gender',
                               limit_choices_to={'section':
                                get_section('entities.entity-gender')},
                               related_name='gender', )
    type = models.ForeignKey(Choice, null=True, blank=False,
                               help_text='Do you want to register as an individual or as a business',
                               limit_choices_to={'section': get_section('entities.entity-type')},
                               related_name='entity_type', )
    age_range = models.ForeignKey(Choice, null=True, blank=True,
                               help_text='Choose your age range',
                               limit_choices_to={'section': get_section('entities.entity-age-range')},
                               related_name='age_range')
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(Choice, null=True, blank=True,
                                help_text='Choose your primary profession',
                                limit_choices_to={'section': get_section('entities.entity-profession')},
                                related_name='entity_profession')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = 'Entities'

    def save(self, *args, **kwargs):
        # self.slug = slugify(extract_username_from_email(str(self.user.username)))
        self.slug = slugify(self.name)
        super(Entity, self).save(*args, **kwargs)

    def get_entity(self):
      return self.id


"""
    Model to display an Interest field
    :Entity
    :website: technology's website.
    :description: technology's description.
"""
class EntityInterest(VBaseModel):
    entity = models.ForeignKey(Entity)
    technology = models.ForeignKey(Technology)

    def __str(self):
      return "(%s %s)" % (self.entity, self.technology)