from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from webapps.modules.core.locations.models import Location
from webapps.modules.tools.admin import BaseAdmin

# class LocationInline(admin.TabularInline):
#     model = Location
#     extra = 1 
#     fields = ('name', 'type', 'is_default', 'sort')

@admin.register(Location)
class LocationAdmin(MPTTModelAdmin, BaseAdmin):
    list_display = ('name', 'type', 'is_default',) 
    list_filter = ('is_default',)
    search_fields = ('scode', 'name')
    fields = ('name', 'parent', 'type', 'is_default', 'sort')
    mptt_indent_field = "name"  
    # inlines = [LocationInline]  


# admin.site.register(Location, LocationAdmin) 