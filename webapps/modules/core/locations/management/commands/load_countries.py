import json
import os
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.management.base import CommandError, BaseCommand
from webapps.modules.core.locations.models import Location
from webapps.modules.tools.models import Choice
from webapps.utils.util_box import gen_tuple_from_dict, get_section


class Command(BaseCommand):
    help = "Create all countries. "
    args = "<code code code code...>"

    def handle(self, *args, **options):
        rel_path = "./countries.json"
        abs_file_path = os.path.join(os.path.dirname(__file__), rel_path)
        mappings = {}

        with open(abs_file_path) as df:
            mappings = json.load(df)

        system_user = Location.get_system_user() 
        type_country = Choice.get_choice('country', get_section('locations.location-type'))
        type_continent = Choice.get_choice('continent', get_section('locations.location-type'))
        type_subregion = Choice.get_choice('sub region', get_section('locations.location-type'))

        for pos, x in enumerate(mappings): 
            print ("Saving %s: %s(%s)" % (pos, x['cca2'], x['name']['official']))
            continent, subregion = None, None

            if x['region']:
                continent, created = Location.objects.get_or_create( 
                                name=x['region'],
                                defaults={ 
                                     'type': type_continent,
                                     'created_by':  system_user, 
                                     'modified_by':  system_user,
                                }
                                )
              

            if x['subregion']:
                subregion, created = Location.objects.get_or_create( 
                                name=x['subregion'],
                                defaults={ 
                                    'type': type_subregion,
                                    'parent': continent,
                                    'created_by': system_user, 
                                    'modified_by': system_user,
                                }
                                )
              

            loc, created = Location.objects.get_or_update( 
                            iso2=x['cca2'], 
                            type=type_country,
                            defaults={ 
                                'iso3': x['cca3'],
                                'name': x['name']['official'],
                                'alias': x['name']['common'],
                                'latitude': (x['latlng'][0] if x['latlng'] else ''),
                                'longitude': (x['latlng'][1] if x['latlng'] else ''),
                                'zip_code': (x['callingCode'][0] if x['callingCode'] else ''),
                                'currency_code': (x['currency'][0] if x['currency'] else ''),
                                'parent': subregion,
                                'created_by': system_user,
                                'modified_by': system_user
                            }) 

          