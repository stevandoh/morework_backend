from decimal import Decimal
from core.locality.models import Country

BASE_COUNTRY = 'GH'


def default_country():
    try:
        return Country.objects.get(is_default=True)
    except Country.DoesNotExist:
        pass

    try:
        return Country.objects.get(iso2__iexact=BASE_COUNTRY)
    except Country.DoesNotExist:
        pass

    return None



def convert_currency(from_curr, to_curr, amount): 
    val, rate = 0, 0
    try:
        from_rate = Country.objects.get(currency_code=from_curr).current_rate()
    except:
        from_rate = None
        
    try:
        to_rate = Country.objects.get(currency_code=to_curr).current_rate()
    except:
        to_rate = None
        
    return convert_currency_with_rate(from_rate, to_rate, amount)


def convert_currency_with_rate(from_rate, to_rate, amount): 
    val, rate = 0, 0 
    
    if from_rate and to_rate:
        rate = to_rate * (1 / from_rate) 
    val = Decimal(amount * rate).quantize(Decimal("0.01")) 
    return {'amt': val, 'rate': rate}

