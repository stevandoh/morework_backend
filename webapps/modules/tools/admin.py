from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
# from mptt.admin import MPTTModelAdmin
from .models import (Choice)
from .forms import ChoiceForm


class BaseAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change): 
        if not change:
            obj.created_by = request.user
        obj.modified_by = request.user
        obj.save() 
        return obj  
        
    def save_formset(self, request, form, formset, change): 
        instances = formset.save(commit=False)
        for obj in instances:
            if not obj.date_added: 
                obj.created_by = request.user
            obj.modified_by = request.user
            obj.save() 
        formset.save_m2m 


@admin.register(Choice)
class ChoiceAdmin(BaseAdmin):
    # list_display = Choice.Attr.list_display
    list_filter = Choice.Attr.list_filter
    list_editable = ['sort']
    prepopulated_fields = {"slug": ("name",)} 
    form = ChoiceForm
    mptt_indent_field = "name"
    search_fields = ['name', 'ext_content_str']
    # fields = ['name', 'slug', 'sort', 'section']
    list_display = ['name', 'sort', 'section']
    actions = ['merge_objects',]

    fieldsets = (
        (None, {
            'fields': ('name', 'section',)
        }),
        ('Advanced options', {
            'classes': ('collapse',),
            'fields': ('slug', 'sort'),
        }),
    )
    
# @admin.register(Technology)
# class TechnologyAdmin(admin.ModelAdmin):
#     pass
