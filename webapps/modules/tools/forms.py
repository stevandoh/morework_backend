from django import forms 
from django.conf import settings 
from .models import Choice


FAKE_CHOICES = ( (1,1), (2,2) )

class ChoiceForm(forms.ModelForm):   
    ext_content_str = forms.MultipleChoiceField(choices=FAKE_CHOICES, label="Content Type", 
        required=False)

    class Meta:
        model = Choice 
        fields = '__all__'
        
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ChoiceForm, self).__init__(*args, **kwargs) 

    def clean(self):
        cd = super(ChoiceForm, self).clean()
        filters = {'name__iexact': cd.get('name', None), 
                   'section': cd.get('section', None), 
                   'slug': cd.get('slug', None)} 
       

        # We do not want to add duplicate choices in same workspace or child spaces
        self.save_option = 'db_save'
       
        return cd
            
    def save(self, commit=False): 
        instance = super(ChoiceForm, self).save(commit=False)   
        if self.save_option == 'skip_save':  
            instance = self.existing_instance
        # else:
        #     instance = instance.save()
        return instance


class AjaxChoiceForm(forms.ModelForm):  
    parent_pk = forms.CharField(required=False, widget=forms.HiddenInput()) 
    
    class Meta:
        model = Choice 
        fields = ['name', 'slug', 'sort', 'parent_pk'] 

    
FAKE_CHOICES = ( (1,1), (2,2))