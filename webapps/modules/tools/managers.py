from django.db import models  
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404


class CoreQuerySet(models.QuerySet):
    def get_or_none(self, **kwargs): 
        try:
            return self.get(**kwargs)
        except:
            return None

class CoreManager(models.Manager): 
    def get_queryset(self):
        return CoreQuerySet(self.model, using=self._db)
    
    def get_or_none(self, **kwargs): 
        try:
            return self.get(**kwargs)
        except:# self.model.DoesNotExist:
            return None
        
    def get_or_update(self, **kwargs):
        defaults = kwargs.pop('defaults', {})
        fields = kwargs 
        obj, created = self.model.objects.get_or_create(defaults=defaults, 
                                                        **fields)
        
        updated = False
        if not created:
            cur_vals = {k:getattr(obj, k) for k in defaults}
            if cur_vals != defaults: 
                for k, v in defaults.items():
                    setattr(obj, k, v)
                obj.save()
                updated = True
        
        return obj, updated

    
class AbstractManager(CoreManager): 

    def get_queryset(self):
        qs = super(AbstractManager, self).get_queryset()  
        return qs#.filter(is_active=True)

    def proxy_queryset(self):
        qs = super(AbstractManager, self).get_queryset()   
        return qs.filter(proxy_content_str=self.model().app_model())


    def for_space(self, workspace=None, qs=None, all_objects=False,
                  show_descendants=False, multi_workspace=None, 
                  use_primary_workspace=False):
        if qs is None:
            if all_objects:
                qs = self.model._default_manager.all()
            else:
                qs = self.get_queryset()

        # We display items based on these criteria 
        # 1. Items that exist in current workspace
        # 2. Items that exist in parent workspace(s)  
        if workspace:
            mw = (multi_workspace if multi_workspace != None else self.model.multi_workspace)
            if mw:
                if show_descendants:
                    workspaces = workspace.get_descendants(include_self=True)
                else:
                    # workspaces = workspace.get_ancestors(ascending=True, 
                    #                                      include_self=True)
                    workspaces = workspace.get_family()

                # Don't show any items in default workspace
                workspaces = workspaces.exclude(is_default=True) 

                if use_primary_workspace:
                    # This is useful for getting transactions in parent 
                    # workspace. Transactions aree normally only shown in 
                    # their primary workspace. For sections that deal in 
                    # multiple workspaces, eg: Transfer, this is needed
                    all_qs = qs.filter(workspace__in=workspaces)  
                else:
                    all_qs = qs.filter(v_object__workspaces__in=workspaces) 
                
                pks = all_qs.values_list('pk', flat=True)
                new_qs = qs.filter(pk__in=set(pks))
                
                return new_qs 
                # return all_qs
            else:
                return qs.filter(workspace=workspace)  
        else:
            return qs.none()

    def workspace_get_or_update(self, workspace, **kwargs):
        defaults = kwargs.pop('defaults', {})
        fields = kwargs
        qs = self.model.objects.for_space(workspace).filter(**fields)
        if qs:
            qs.update(**defaults)
            return qs[0], False
        else:
            fields.update({'workspace': workspace})
            return self.model.objects.get_or_update(defaults=defaults, **fields)

    def get_for_space(self, workspace, **kwargs):
        defaults = kwargs.pop('defaults', {})
        fields = kwargs
        qs = self.model.objects.for_space(workspace)
        qs = qs.filter(**kwargs)
        try:
            obj = qs.get(**kwargs) 
        except self.model.MultipleObjectsReturned:
            obj = qs[0]
        except self.model.DoesNotExist:
            obj = None
        
        return obj

    def get_for_space_or_404(self, workspace, **kwargs):
        defaults = kwargs.pop('defaults', {})
        fields = kwargs
        qs = self.model.objects.for_space(workspace)
        try:
            obj = qs.get(**kwargs) 
        except self.model.DoesNotExist:
            verbose = self.model._meta.verbose_name_plural.decode()
            filter_txt = ', '.join(['%s=%s' % (k, v) for k, v in kwargs.items()])
            raise Http404('No %s found with %s' % (verbose, filter_txt)) 
        return obj
