# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-10 20:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=100, null=True)),
                ('sort', models.PositiveIntegerField(default=1000)),
                ('section', models.CharField(choices=[('config.attribute-type', 'Config Attribute Type'), ('entities.entity-category', 'Entity Category'), ('entities.entity-gender', 'Gender'), ('entities.entity-title', 'Title'), ('entities.entity-type', 'Entity Type')], max_length=100)),
                ('ext_content_str', models.CharField(blank=True, max_length=100, null=True, verbose_name='Content Type')),
                ('is_reserved', models.BooleanField(default=False)),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='tools.Choice')),
            ],
            options={
                'ordering': ('section', 'sort', 'name'),
            },
            managers=[
                ('tree', django.db.models.manager.Manager()),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='choice',
            unique_together=set([('name', 'parent', 'slug', 'section')]),
        ),
    ]
