# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-09 17:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0003_auto_20170307_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='section',
            field=models.CharField(choices=[('config.attribute-type', 'Config Attribute Type'), ('entities.entity-age-range', 'Age Range'), ('entities.entity-category', 'Entity Category'), ('entities.entity-gender', 'Gender'), ('entities.entity-title', 'Title'), ('entities.entity-type', 'Entity Type'), ('jobs.job-advert-type', 'Job Advert Type'), ('jobs.job-type', 'Job Type'), ('jobs.job-venue', 'Job Venue'), ('locations.location-type', 'Location Type'), ('technologies-type', 'Technology Type')], max_length=100),
        ),
    ]
