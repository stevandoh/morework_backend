
EMPLOYMENT_STATUS_CHOICES = (
        ('EMPLOYED', 'Employed'),
        ('UNEMPLOYED', 'Unemployed'),
        ('SELF_EMPLOYED', 'Self Employed'),
    )